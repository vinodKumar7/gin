//
//  ProductDetailViewController.h
//  GrabItNow
//
//  Created by vairat on 25/05/15.
//  Copyright (c) 2015 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressCell.h"
#import "ProductDataParser.h"
#import "MerchantListParser.h"
#import "RedeemXMLparser.h"


@interface ProductDetailViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate,ProductXMLParserDelegate,  MerchantXMLParserDelegate, RedeemXMLparserDelegate>
{
    int phnoCount;
    NSMutableArray *phMutableArray;
    NSArray *phArray;
    
    CLLocationCoordinate2D Location;
}

@property (strong, nonatomic) IBOutlet UICollectionView *productCollectionView;

@property (strong, nonatomic) AddressCell *cCell;
@property (strong, nonatomic) NSString *product_ID;

- (IBAction)headerBtnPressed:(id)sender;

@end
