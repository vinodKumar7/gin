//
//  AppDelegate.h
//  GrabItNow
//
//  Created by MyRewards on 11/24/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "HomeViewController.h"
#import "Product.h"
#import "ParkingTimer.h"
#import "NoticeBoard.h"
#import <AVFoundation/AVFoundation.h>
@class LoginViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate,AVAudioPlayerDelegate,UIAlertViewDelegate> {
    
    
    NSTimer *CountTimer;
    NSTimer *CountDownTimer;
    int countDownTime;
    int actualTime;
    UILabel *label;
    int temp;
    int remndr;
    int x; //x for logout reminder storing value
    
    NSDate *closedTime;
    AVAudioPlayer *player;
    NSInteger badgecount;
    
}
@property (nonatomic, readwrite) int temp;
@property (nonatomic, readwrite) int remndr;
@property (nonatomic, readwrite) int x;
@property (nonatomic, readwrite) BOOL isNoticeBoardBground;
@property  (nonatomic, strong)NSMutableArray *refArray;

@property(nonatomic, readwrite)NSInteger badgecount;
@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) User *sessionUser;
@property (strong, nonatomic) LoginViewController *viewController;
@property (strong, nonatomic) HomeViewController *homeViewController;


-(void)startCountDownTimerWithTime:(int)time andUILabel:(UILabel *)currentLabel;
-(void)invalidateCurrentCountDownTimer;
- (void) showhomeScreen;

- (NSString *) sessionUsername;
- (NSString *) sessionPassword;
- (NSString *) clientDomainName;

- (void) logoutUserSession;
- (BOOL) isIphone5;
- (void) loginSuccessfulWithUserdetails:(User *) userDetails password:(NSString *) pWord;

// My Favorite methods
- (BOOL) addProductToFavorites:(Product *) product;
- (BOOL) removeProductFromfavorites:(Product *) product;
- (BOOL) productExistsInFavorites:(Product *) product;
- (NSArray *) myFavoriteProducts;

//Notice Board methods
- (BOOL) addNoticeBoardID:(NoticeBoard *) nb;
- (BOOL) removeNoticeboard:(NoticeBoard *) nb;
- (BOOL) nbExistsInCoredata:(NoticeBoard *) nb;
- (NSArray *) noticeBoardIds;



//My Parking remainder Methods
-(BOOL) addRemainder:(ParkingTimer *) remainder;
- (ParkingTimer *) FetchRemainder;
- (BOOL) clearRemainder;



// **** Core data properties
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory;

@end
