//
//  ProductDetailViewController.m
//  GrabItNow
//
//  Created by vairat on 25/05/15.
//  Copyright (c) 2015 MyRewards. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "AppDelegate.h"
#import "DMLazyScrollView.h"
#import "MyCollectionViewCell.h"
#import "AddressCell.h"
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"
#import "Merchant.h"
#import "Product.h"

@interface ProductDetailViewController () <DMLazyScrollViewDelegate>
{
    AppDelegate *appDelegate;
    DMLazyScrollView* lazyScrollView;
    NSMutableArray*    viewControllerArray;
    
    UIView *productRedeemView, *contactsView;
    UITableView *addressTableView;
    
    NSInteger currentBtnTag;
    UIButton *currentButton;
    UIButton *previousButton;
    NSInteger previousSelection;
    
    int contactLabelOrigin_Y;
    NSString *callNumber;
    
    ASIFormDataRequest *getProductRequest;
    ASIFormDataRequest *redeemstatusRequest;
    ASIFormDataRequest *redeemRequest;
    ASIFormDataRequest * merchantFetchRequest;
//    RedeemXMLparser *redeemparser;
    ProductDataParser *productDataXMLParser;
    Product *currentProduct;
//    Redeem *currentRedeem;
    
    NSMutableArray *merchantList;
    
    Merchant *merchant;
    MerchantListParser *merchantListParser;
}

@end

@implementation ProductDetailViewController
@synthesize productCollectionView;
@synthesize cCell;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setTranslucent:NO];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.homeViewController hideBackButon:NO];
    
    [productCollectionView registerNib:[UINib nibWithNibName:@"MyCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
   
    currentBtnTag = 1;
    [self pageSetting];
    [lazyScrollView setPage:5 transition:FORWARD animated:NO];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@get_product.php?id=%@",URL_Prefix,_product_ID];
    NSLog(@"URL is %@ ",urlString);
    NSURL *url= [NSURL URLWithString:urlString];
    getProductRequest = [ASIFormDataRequest requestWithURL:url];
    [getProductRequest setDelegate:self];
    [getProductRequest startAsynchronous];
    
    [self fetchMerchantWithProductID:currentProduct.productId];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [appDelegate.homeViewController setHeaderTitle:@"PRODUCT DETAILS"];
}

- (void) viewDidAppear:(BOOL)animated
{
    
}
- (void)pageSetting
{
    
    // PREPARE PAGES
    NSUInteger numberOfPages = 6;
    [viewControllerArray removeAllObjects];
    viewControllerArray = [[NSMutableArray alloc] initWithCapacity:numberOfPages];
    for (NSUInteger k = 0; k < numberOfPages; k++) {
        [viewControllerArray addObject:[NSNull null]];
    }
    
    // PREPARE LAZY VIEW
    lazyScrollView = Nil;
    lazyScrollView = [[DMLazyScrollView alloc] initWithFrame:CGRectMake(0, 0, productCollectionView.bounds.size.width, self.productCollectionView.bounds.size.height)];
    lazyScrollView.tag = 1222;
    
    lazyScrollView.dataSource = ^(NSUInteger index) {
        
        return [self controllerAtIndex:index];
        
        
    };
    
    lazyScrollView.numberOfPages = numberOfPages;
    lazyScrollView.controlDelegate = self;
    [productCollectionView.collectionViewLayout invalidateLayout];
}

- (UIViewController *) controllerAtIndex:(NSInteger) index {
    
    //NSLog(@"---------------------controllerAtIndex %ld-------------------", (long)index);
    if (index > viewControllerArray.count || index < 0) return nil;
    
    id res = [viewControllerArray objectAtIndex:index];
    if (res == [NSNull null]) {
        
        UIViewController *contr = [[UIViewController alloc] init];
        contr.view.backgroundColor = [UIColor whiteColor];
        
        switch (index) {
                
                
            case 0:
                [contr.view addSubview:[self prepareTextToDisplay:currentProduct.productDesciption andHeadding:currentProduct.productOffer]];
            
                break;
           
            case 1:
            {
                UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, productCollectionView.bounds.size.width, 700)];
                view1.backgroundColor = [UIColor redColor];
                [contr.view addSubview:view1];
            }

                
                break;
                
            case 2:
                
            {
//                addressTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, productCollectionView.bounds.size.width, productCollectionView.bounds.size.height) style:UITableViewStylePlain];
//                addressTableView.backgroundColor = [UIColor whiteColor];
//                addressTableView.dataSource = self;
//                addressTableView.delegate = self;
//                [contr.view addSubview:addressTableView];
            }
                
                break;
            case 3:
                if([currentProduct.productTermsAndConditions length]>0)
                    [contr.view addSubview:[self prepareTextToDisplay:currentProduct.productTermsAndConditions andHeadding:@"Terms And Conditions"]];
                else
                    [contr.view addSubview:[self prepareTextToDisplay:@"" andHeadding:@"No Terms And Conditions"]];
    
                break;
                
            case 4:
                contactsView= [[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f, productCollectionView.bounds.size.width, 266.0f)];
                contactsView.backgroundColor = [UIColor whiteColor];
                [contr.view addSubview:contactsView];
                [self updateContactsView];
                break;
            case 5:
            {
                UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, productCollectionView.bounds.size.width, productCollectionView.bounds.size.height)];
                view1.backgroundColor = [UIColor yellowColor];
            
                [contr.view addSubview:view1];
            }
                
                break;
                
            default:
                break;
        }
        
        
        
        
        [viewControllerArray replaceObjectAtIndex:index withObject:contr];
        return contr;
    }
    return res;
}


-(UIWebView *)prepareTextToDisplay:(NSString *)content andHeadding: (NSString *)header{
    
    
    UIWebView  *containerWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0.0f,0.0f, productCollectionView.bounds.size.width, 566.0f)];
    
    containerWebView.userInteractionEnabled = NO;
    containerWebView.backgroundColor = [UIColor whiteColor];
    
    NSString *resultText = @"";
    
    resultText =       [NSString stringWithFormat:@"<html> \n"
                        
                        "<head> \n"
                        "<style type=\"text/css\"> \n"
                        "body {font-family: \"%@\"; font-size: \"%@ \";}\n"
                        "H4{ color: rgb(198,38,21) }\n"
                        "</style> \n"
                        "<style type='text/css'>body { max-width: 300%; width: auto; height: auto; }</style>"
                        "</head> \n"
                        "<body><H4>%@</H4>%@</body> \n"
                        "</html>", @"Helvetica", [NSNumber numberWithInt:10],header, content];
    
    [containerWebView loadHTMLString:resultText baseURL:nil];
    return containerWebView;
}

#pragma mark - lazyScrollView Delegate Methods

- (void)lazyScrollViewDidEndDecelerating:(DMLazyScrollView *)pagingView atPageIndex:(NSInteger)pageIndex
{
    
    
    UIButton *button=[[UIButton alloc]init];
    button.tag=pageIndex+1;
    
//    [self performSelector:@selector(btnPressed:) withObject:button afterDelay:0.03];
    
}



- (void)lazyScrollViewDidEndDragging:(DMLazyScrollView *)pagingView{
    
    [productCollectionView.collectionViewLayout invalidateLayout];
    
}


- (void)lazyScrollView:(DMLazyScrollView *)pagingView currentPageChanged:(NSInteger)currentPageIndex{
    
    
    // NSLog(@"currentPageIndex::%ld", (long)currentPageIndex);
    currentBtnTag = currentPageIndex+1;
    
    // NSLog(@"lazyscroll view width is %f",pagingView.bounds.size.width);
}

- (void) updateContactsView {
    
    NSString* phnoString = [currentProduct.phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    phArray = [[NSArray alloc]init];
    phMutableArray = [[NSMutableArray alloc]init];
    
    //    phArray = [phnoString componentsSeparatedByString:@"/,"];
    phArray = [phnoString componentsSeparatedByCharactersInSet:
               [NSCharacterSet characterSetWithCharactersInString:@"/,"]
               ];
    NSLog(@"strings %@",phArray);
    
    if([phArray count] !=  0)
    {
        contactLabelOrigin_Y = 10;
        for (int i = 0; i < [phArray count]; i++) {
            
            UILabel *contactlabel = [[UILabel alloc]initWithFrame:CGRectMake(60, contactLabelOrigin_Y, 150, 30)];
            [contactsView addSubview:contactlabel];
            
            if ([[phArray objectAtIndex:i] length] <= 4) {
                
                NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:[[phArray objectAtIndex:i-1] length] - [[phArray objectAtIndex:i] length]];
                
                for (int j=0; j < [[phArray objectAtIndex:i-1] length] - [[phArray objectAtIndex:i] length]; j++) {
                    NSString *ichar  = [NSString stringWithFormat:@"%c", [[phArray objectAtIndex:i-1] characterAtIndex:j]];
                    [characters addObject:ichar];
                }
                
                NSString * newString = [[characters valueForKey:@"description"] componentsJoinedByString:@""];
                contactlabel.text = [newString stringByAppendingString:[phArray objectAtIndex:i]];
                
                [phMutableArray addObject:contactlabel.text];
            }
            else
            {
                contactlabel.text = [phArray objectAtIndex:i];
                [phMutableArray addObject:[phArray objectAtIndex:i]];
            }
            
            
            UIButton *contactlabel_Button = [[UIButton alloc]initWithFrame:CGRectMake(30, contactLabelOrigin_Y+3, 25, 25)];
            contactlabel_Button.tag = i;
            NSLog(@"contact btn tag %ld",(long)[contactlabel_Button tag]);
            [contactlabel_Button setBackgroundImage:[UIImage imageNamed:@"phones.png"] forState:UIControlStateNormal];
            [contactsView addSubview:contactlabel_Button];
            
            
            [contactlabel_Button addTarget:self action:@selector(callButtonpressed:)forControlEvents:UIControlEventTouchUpInside];
            contactlabel_Button.userInteractionEnabled = YES;
            
            
            
            //            [contactBtn setBackgroundImage:[UIImage imageNamed:@"btn3.png"] forState:UIControlStateNormal];
            
            
            
            contactLabelOrigin_Y = contactLabelOrigin_Y + 30;
        }
        
    }
    else
    {
        UILabel *contactsLabel  = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 150, 50)];
        
        contactsLabel.text = @"No contact Number Available";
        
        
        contactsLabel.numberOfLines = 0;
        contactsLabel.font = [UIFont systemFontOfSize:15.0];
        [contactsView addSubview:contactsLabel];
        
    }
    
}
-(IBAction)callButtonpressed:(id)sender
{
    NSLog(@"call button pressed");
    
    
    callNumber = [phMutableArray objectAtIndex:[sender tag]];
    
    UIAlertView *call_Alert = [[UIAlertView alloc]initWithTitle:@"Call" message:callNumber delegate:self cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel" , nil];
    call_Alert.tag = 111;
    [call_Alert show];
    
    
}

#pragma mark - CollectionView DataSource And Delegate Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}

- (MyCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
       MyCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
      [cell addSubview:lazyScrollView];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    

    
    
    switch (currentBtnTag) {
        case 1:
            return CGSizeMake(productCollectionView.bounds.size.width, 700);
            break;
        case 2:
            
            return CGSizeMake(productCollectionView.bounds.size.width, productCollectionView.bounds.size.height);
            
            break;
        case 3:
            
            return CGSizeMake(productCollectionView.bounds.size.width, productCollectionView.bounds.size.height);
            
            break;
        case 4:
            
            return CGSizeMake(productCollectionView.bounds.size.width, productCollectionView.bounds.size.height);
            
            break;
        case 5:
            
            
            return CGSizeMake(productCollectionView.bounds.size.width, productCollectionView.bounds.size.height);
            
            break;
        case 6:
            
            
            return CGSizeMake(productCollectionView.bounds.size.width, productCollectionView.bounds.size.height);
            
            break;
    }

      return CGSizeMake(0, 0);
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)headerBtnPressed:(id)sender {
    
    currentBtnTag = [sender tag];
    
    [productCollectionView.collectionViewLayout invalidateLayout];
    
    if(productCollectionView.contentOffset.y>0)
    {
        
        [productCollectionView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    
    currentButton = sender;
    
//    [self setHighlightButtonImage:[sender tag]];
    
    if (previousSelection < [currentButton tag])
        [lazyScrollView setPage:[currentButton tag]-1 transition:FORWARD animated:NO];
    else
        [lazyScrollView setPage:[currentButton tag]-1 transition:BACKWARD animated:NO];
}

#pragma mark- Request Finished and Request Failed

- (void)requestFinished:(ASIHTTPRequest *)request
{
    if(request == getProductRequest){
        NSLog(@"=======>> Product:: %@",[request responseString]);
        NSXMLParser *productParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productDataXMLParser = [[ProductDataParser alloc] init];
        productDataXMLParser.delegate = self;
        productParser.delegate = productDataXMLParser;
        [productParser parse];
        
    }
    
    else if (request == redeemstatusRequest)
    {
        //NSLog(@"redeemstatus response %@ ",[request responseString]);
//        NSXMLParser *countParser = [[NSXMLParser alloc] initWithData:[request responseData]];
//        redeemparser=[[RedeemXMLparser alloc]init];
//        redeemparser.delegate = self;
//        countParser.delegate=redeemparser;
//        [countParser parse];
        //[activityView removeFromSuperview];
    }
    
    else  {
        // NSLog(@"Merchant ** RES: %@",[request responseString]);
        
        NSXMLParser *merchantParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        merchantListParser = [[MerchantListParser alloc] init];
        merchantListParser.delegate = self;
        merchantParser.delegate = merchantListParser;
        [merchantParser parse];
        
        merchantFetchRequest = nil;
        // addressDetailsLoaded = YES;
    }
    
    
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                         message:@"Server Busy"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:@"Cancel",nil];
    [alert_view show];
    
    
    
}


#pragma mark- fetch Address

- (void) fetchMerchantWithProductID:(NSString *) productId {
    
    if (merchantFetchRequest) {
        // Already a request is in progress.
        return;
    }
    
    
    // Make server call for more products.
    NSString *urlString = [NSString stringWithFormat:@"%@get_product_addresses.php?pid=%@",URL_Prefix,_product_ID];
    
    NSLog(@"Fetch Merchants URL: %@",urlString);
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    merchantFetchRequest = [ASIFormDataRequest requestWithURL:url];
    [merchantFetchRequest setDelegate:self];
    [merchantFetchRequest startAsynchronous];
    
    
}

#pragma mark- Parsing Methods

- (void)parsingProductDataFinished:(Product *)product
{
    
    currentProduct = product;
//    redeem_psw     = currentProduct.redeemPassword;
//    redeem_Target   = [currentProduct.redeemTarget intValue];
    [self redeemStatusRequestMethod];
    
}

- (void)parsingProductDataXMLFailed
{
    
}

- (void) parsingcountFinished:(Redeem *) redeemObj
{
    
    int originalRedeemCount = [redeemObj.count intValue];
    NSLog(@"--------------------------------------------------------------------------------------");
    NSLog(@"originalRedeemCount is::: %d ------- [currentProduct.redeemTarget intValue] is:: %d ",originalRedeemCount,[currentProduct.redeemTarget intValue]);
    
    
    
    
    int redeemTarget;
    //    if(originalRedeemCount >= [currentProduct.redeemTarget intValue])
    //    {
    //        redeemTarget = [currentProduct.redeemTarget intValue];
    //
    //        while (originalRedeemCount >= redeemTarget)
    //            originalRedeemCount = originalRedeemCount - redeemTarget;
    //
    //
    //    }
    //
    //    NSLog(@" modified RedeemCount::%d",originalRedeemCount);
    //    currentProduct.currentRedeemCount = [NSString stringWithFormat:@"%d", originalRedeemCount];
    
//    
//    [self pageSetting];
//    
//    isFromParsingCountFinished = YES;
//    [_collectionView reloadData];
//    
//    
//    [lazyScrollView setPage:4 transition:FORWARD animated:NO];
    
    
}
- (void) RedeemXMLparsingFailed{}

-(void)redeemStatusRequestMethod{
    
//    NSString *rdmurl =[NSString stringWithFormat:@"%@redeem_status.php?user_id=%@&pid=%@",URL_Prefix,[self deviceUDID],currentProduct.productId];
//    NSURL *redeemurl= [NSURL URLWithString:rdmurl];
//    redeemstatusRequest=[ASIFormDataRequest requestWithURL:redeemurl];
//    [redeemstatusRequest setDelegate:self];
//    [redeemstatusRequest startAsynchronous];
    
    
}

- (void)parsingMerchantListFinished:(NSArray *)merchantsListLocal {
    
    
    if (!merchantList)
        merchantList = [[NSMutableArray alloc] initWithArray:merchantsListLocal];
    
    else
        [merchantList addObjectsFromArray:merchantsListLocal];
    
    [addressTableView reloadData];
}


- (void)parsingMerchantListXMLFailed {
    
}

#pragma mark- AlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    if (alertView.cancelButtonIndex == buttonIndex)
//    {
//        if (alertView.tag == 21) {
//            // Add to fav case
//            BOOL success = [appDelegate addProductToFavorites:currentProduct];
//            
//            [self updateFavoriteStatus];
//            if(success)
//            {
//                NSLog(@"product added");
//            }
//            
//            //        if (success && delegate && [delegate respondsToSelector:@selector(productAddedToFavorites:)]) {
//            //            [delegate productAddedToFavorites:currentProduct];
//            //        }
//            
//        }
//        else if (alertView.tag == 22) {
//            // Remove from fav case
//            BOOL success = [appDelegate removeProductFromfavorites:currentProduct];
//            
//            [self updateFavoriteStatus];
//            
//            //        if (success && delegate && [delegate respondsToSelector:@selector(productremovedFromFavorites:)]) {
//            //            [delegate productremovedFromFavorites:currentProduct];
//            //        }
//            
//        }
//        
//    }
//    if (alertView.tag == 111) {
//        NSString *phoneURLString = callNumber;
//        NSString *newString = [ phoneURLString stringByReplacingOccurrencesOfString:@" " withString:@""];
//        
//        NSURL *phoneURL = [NSURL URLWithString:newString];
//        [[UIApplication sharedApplication] openURL:phoneURL];
//    }
//    
    
    
}

- (void) updateFavoriteStatus {
//    if ([appDelegate productExistsInFavorites:currentProduct]) {
//        //[favButton setTitle:@"Remove favorite" forState:UIControlStateNormal];
//        [favouriteButton setImage:[UIImage imageNamed:@"heart_full.png"] forState:UIControlStateNormal];
//    }
//    else {
//        //[favButton setTitle:@"Add to favorite" forState:UIControlStateNormal];
//        [favouriteButton setImage:[UIImage imageNamed:@"heart_empty.png"] forState:UIControlStateNormal];
//    }
}



#pragma mark- TableView DataSource and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [merchantList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CellIdentifier";
    
    AddressCell *cell;
    if (cell == nil) {
        
        cell = (AddressCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    
    Merchant *aMerchant = [merchantList objectAtIndex:indexPath.row];
    
    
    
    if([aMerchant.mMailAddress1 length] == 0  && aMerchant.mState.length ==0){
        
        cell.lblAddress.text = @"No Addresses Available";
        cell.mapButton.hidden = YES;
        cell.mapLabel.hidden = YES;
    }
    else{
        
        NSString * str =aMerchant.mMailAddress1 ;
        
        if( [aMerchant.mMailSuburb isEqualToString:@"(null)"] )
        {
            // NSLog(@"Address: %@",aMerchant.mMailAddress1);
            // str=@"trimmed";
            
        }
        else
        {
            NSLog(@"null: %@",[NSNull null]);
            str = [str stringByAppendingString:[self appendStringWithNewline:aMerchant.mMailSuburb]];
        }
        if([aMerchant.mState length]>0 && [aMerchant.mPostCode length]>0)
            str = [NSString stringWithFormat:@"%@, %@-%@",str,aMerchant.mState,aMerchant.mPostCode];
        else if([aMerchant.mState length]>0)
            str = [NSString stringWithFormat:@"%@, %@",str,aMerchant.mState];
        if([aMerchant.mPhone length]>0 ){
            NSString *phno = [NSString stringWithFormat:@"Ph: %@",aMerchant.mPhone];
            str = [str stringByAppendingString:[self appendStringWithNewline:phno]];
        }
        // NSString *deviceName = @"Kenny's iPhone";
        NSLog(@"non--stripped1 is...%@ ",str  );
        NSString *stripped = [str stringByReplacingOccurrencesOfString:@"(null)," withString:@""];
        NSString *stripped1 = [stripped stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
        cell.lblAddress.text = stripped1;
        
    }
    cell.lblAddress.textColor = [UIColor blackColor];
    Location.latitude = aMerchant.coordinate.latitude;
    Location.longitude = aMerchant.coordinate.longitude;
    NSLog(@"latitude and longitude is.. %f,%f",Location.latitude,Location.longitude);
    
    cell.mapButton.tag = indexPath.row +100;
    [cell.mapButton addTarget:self action:@selector(mapButtonpressed:)forControlEvents:UIControlEventTouchUpInside];
    cell.mapButton.userInteractionEnabled = YES;
    if ( (Location.latitude == 0.000000)|| (Location.longitude == 0.000000) || [cell.lblAddress.text isEqualToString:@"No Addresses Available"])
    {
        if (indexPath.row > 0) {
            //            cell.lblAddress.text = @"";
        }
        cell.mapButton.hidden = YES;
        cell.mapLabel.hidden = YES;
        
        
    }
    else
    {
        cell.mapButton.hidden = NO;
        cell.mapLabel.hidden = NO;
        
        
    }
    //    cell.mapButton.tag = indexPath.row;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

-(IBAction)mapButtonpressed:(id)sender
{
    NSLog(@"=====MAPBUTTON PRESSED=====tag is %ld ",(long)[sender tag]);
    Merchant *aMerchant = [merchantList objectAtIndex:[sender tag] - 100];
    Location.latitude = aMerchant.coordinate.latitude;
    Location.longitude = aMerchant.coordinate.longitude;
    NSLog(@"latitude and longitude is.. %f,%f",Location.latitude,Location.longitude);
    
    
    
    NSURL *addressUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com?q=%f,%f",Location.latitude, Location.longitude, nil]];
    // NSLog(@"addressUrl  is ....%@",addressUrl);
    
    [[UIApplication sharedApplication] openURL:addressUrl];
}

-(NSString *)appendStringWithNewline:(NSString *)str
{
    NSString *appendStr = [NSString stringWithFormat:@"\n%@",str];
    return appendStr;
}

@end
