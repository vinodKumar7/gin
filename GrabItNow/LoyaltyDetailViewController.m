//
//  LoyaltyDetailViewController.m
//  CoffeeApp
//
//  Created by vairat on 28/03/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import "LoyaltyDetailViewController.h"
#import "CollectionViewManager.h"
#import "KeyboardViewController.h"
#import "AppDelegate.h"
#import <AdSupport/ASIdentifierManager.h>

@interface LoyaltyDetailViewController (){
    
    AppDelegate *appDelegate;
    BOOL controlCameFromKeyboardView;
    BOOL isLastStamp;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong,nonatomic) CollectionViewManager *collectionViewManager;
@property (strong,nonatomic) KeyboardViewController *keyboardViewControllerObj;


@end

@implementation LoyaltyDetailViewController
@synthesize keyboardViewControllerObj;
@synthesize productImage, product_id, product_Name,redeemButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        

    }
    return self;
}
- (BOOL)prefersStatusBarHidden
{
    return NO;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
//    [[self navigationController] setNavigationBarHidden:NO animated:NO];
   
    [self.navigationController.navigationBar setTranslucent:NO];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.homeViewController hideBackButon:NO];
    
    
    UIBarButtonItem *flipButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Back"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(back)];
    self.navigationItem.leftBarButtonItem = flipButton;

    
    self.collectionViewManager                = [[CollectionViewManager alloc]init];
    self.collectionViewManager.product_ID     = product_id;
    self.collectionViewManager.proImage       = productImage;
    self.collectionViewManager.redeem_Button  = self.redeemButton;
    self.collectionViewManager.collectionView = self.collectionView;
    isLastStamp = NO;
    

}
-(void)back{
    
    isLastStamp = NO;
    self.collectionViewManager.favouriteButton.hidden = YES;
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void) viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [appDelegate.homeViewController setHeaderTitle:@"PRODUCT DETAILS"];
    
    self.collectionViewManager.stampTapCount = 0;
    self.collectionViewManager.selectedStampCount = [NSString stringWithFormat:@"1"];
    if (controlCameFromKeyboardView == YES) {
        
        controlCameFromKeyboardView = NO;
        self.collectionViewManager.favouriteButton.hidden = NO;
        
        [self.collectionViewManager redeemButtonPressed];
        
    }


}

-(void)viewWillDisappear:(BOOL)animated
{
    _collectionViewManager.favouriteButton.hidden=YES;
    
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
//    self.collectionViewManager.stampTapCount = (self.collectionViewManager.stampTapCount == 0)?1:self.collectionViewManager.stampTapCount;
//    self.collectionViewManager.selectedStampCount = [NSString stringWithFormat:@"%d",self.collectionViewManager.stampTapCount];
//    
//    if(self.collectionViewManager.checkedStamps+self.collectionViewManager.stampTapCount+1 == self.collectionViewManager.redeem_Target || self.collectionViewManager.stampTapCount == self.collectionViewManager.redeem_Target -1){
//        
//        self.collectionViewManager.selectedStampCount = [NSString stringWithFormat:@"%d",self.collectionViewManager.stampTapCount+1];
//        NSLog(@"case 4 : checkedStamps+1 == [currentProduct.redeemTarget integerValue]-1");
//        
//        isLastStamp = YES;
//        
//    }
//   
//    
//    if ([segue.identifier isEqualToString:@"KeyBoardVC"])
//    {
//
//        KeyboardViewController *kvc = segue.destinationViewController;
//        kvc.product_ID    = product_id;
//        kvc.product_Image = productImage;
//        kvc.product_Name  = product_Name;
//        kvc.deviceID = [self deviceUDID];
//        kvc.isLastStamp = isLastStamp;
//        kvc.redeemPassword = self.collectionViewManager.redeem_psw;
//        kvc.multipleRedeemCount = self.collectionViewManager.selectedStampCount;
//       
//        NSLog(@"self.collectionViewManager.selectedStampCount:: %@",self.collectionViewManager.selectedStampCount);
//        controlCameFromKeyboardView = YES;
//    }
}

-(NSString *)deviceUDID
{
    
    NSString *uid;
    if([ [ UIScreen mainScreen ] bounds ].size.height == 568)
    {
        uid= [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        
    }
    else
    {
        uid = [self advertisingIdentifier];
        if ([uid isKindOfClass:NULL]) {
            uid = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
        }
        
    }
    
    
    return uid;
}
- (NSString *) advertisingIdentifier
{
    if (!NSClassFromString(@"ASIdentifierManager")) {
        SEL selector = NSSelectorFromString(@"uniqueIdentifier");
        if ([[UIDevice currentDevice] respondsToSelector:selector]) {
            return [[UIDevice currentDevice] performSelector:selector];
        }
        
    }
    return [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)redeemBtnPressed:(id)sender {
    
    self.collectionViewManager.stampTapCount = (self.collectionViewManager.stampTapCount == 0)?1:self.collectionViewManager.stampTapCount;
    self.collectionViewManager.selectedStampCount = [NSString stringWithFormat:@"%d",self.collectionViewManager.stampTapCount];
    
    if(self.collectionViewManager.checkedStamps+self.collectionViewManager.stampTapCount+1 == self.collectionViewManager.redeem_Target || self.collectionViewManager.stampTapCount == self.collectionViewManager.redeem_Target -1){
        
        self.collectionViewManager.selectedStampCount = [NSString stringWithFormat:@"%d",self.collectionViewManager.stampTapCount+1];
        NSLog(@"case 4 : checkedStamps+1 == [currentProduct.redeemTarget integerValue]-1");
        
        isLastStamp = YES;
        
    }
    
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    KeyboardViewController *kvc = [storyboard instantiateViewControllerWithIdentifier:@"keyboardViewController"];
    

//    KeyboardViewController *kvc = segue.destinationViewController;
    kvc.product_ID    = product_id;
    kvc.product_Image = productImage;
    kvc.product_Name  = product_Name;
    kvc.deviceID = [self deviceUDID];
    kvc.isLastStamp = isLastStamp;
    kvc.redeemPassword = self.collectionViewManager.redeem_psw;
    kvc.multipleRedeemCount = self.collectionViewManager.selectedStampCount;
    
    NSLog(@"self.collectionViewManager.selectedStampCount:: %@",self.collectionViewManager.selectedStampCount);
    controlCameFromKeyboardView = YES;
    
    [self.navigationController pushViewController:kvc animated:NO];
}
@end
