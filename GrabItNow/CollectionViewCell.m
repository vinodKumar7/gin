//
//  CollectionViewCell.m
//  PDKTStickySectionHeadersCollectionViewLayoutDemo
//
//  Created by Daniel García on 31/12/13.
//  Copyright (c) 2013 Produkt. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell
@synthesize titleLabel, subtitleLabel, imageView, bgImageView, cellBackgroundView;
- (void)awakeFromNib{
    [super awakeFromNib];
}

@end
