//
//  AppDelegate.m
//  GrabItNow
//
//  Created by MyRewards on 11/24/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "HomeViewController.h"
#import <CoreData/CoreData.h>
#import <Parse/Parse.h>

#define Login_Token_Delimiter @""
#define Login_Token_Expiry_period (7*24*60*60)//1*60
#define Login_Token_Username_Key @"Login_Token_Username_Key"
#define Login_Token_Password_Key @"Login_Token_Password_Key"
#define Login_Token_Timestamp_Key @"Login_Token_Timestamp_Key"
#define User_object_Key @"User_object"

@interface AppDelegate()
- (User *) retrieveUserDetailsFromUserDefaults;
@end

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

@synthesize sessionUser;
@synthesize window;
@synthesize viewController;
@synthesize homeViewController;
@synthesize temp;
@synthesize remndr;
@synthesize x;
@synthesize isNoticeBoardBground;
@synthesize refArray;
@synthesize badgecount;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    NSLog(@"Screen Bounds: %@",NSStringFromCGRect([[UIScreen mainScreen] bounds]));
    
    //============================= pushnotification code ===========================//
    [Parse setApplicationId:@"yb4VkGeiRtxdWtI5phNeCHG0s5Eh24DTCvf3MqAP"
                  clientKey:@"Yz3v1RGgrOZ3wL1LnuvVIwvSwU6PeslUSQA53Q1h"];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    
    // Register for push notifications
    [application registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeBadge |
     UIRemoteNotificationTypeAlert |
     UIRemoteNotificationTypeSound];
    
    
    //==================================================================================
     badgecount = 0;
    isNoticeBoardBground = NO;
    self.refArray = [[NSMutableArray alloc]init];
    if (![self isFirstTimeLogin]) {
        
        // Fetch user details
        self.sessionUser = [self retrieveUserDetailsFromUserDefaults];
        
        // check for token validity.
        if ([self isLoginTokenValid]) {
            [self showhomeScreen];
        }
        else {
            // If token expired, renew automatically.(i.e login user again automatically using users credentials previously stored)
            [self renewTheLoginToken];
        }
    }
    else {
        // Login user
        [self showRegistrationScreen];
    }
    
    return YES;
}

- (BOOL) isIphone5 {
    
    if( [ [ UIScreen mainScreen ] bounds ].size.height == 568.0){
        
        NSLog(@"====IPHONE5====");
        return YES;
    }
    NSLog(@"====IPHONE4====");
    return NO;
}



- (BOOL) isFirstTimeLogin {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:Login_Token_Timestamp_Key]) {
        return NO;
    }
    
    return YES;
}

- (BOOL) isLoginTokenValid {
    
    NSString *timeStampString = [[NSUserDefaults standardUserDefaults] objectForKey:Login_Token_Timestamp_Key];
    
    if(timeStampString) {
        
        NSDateFormatter *dateForm = [[NSDateFormatter alloc] init];
        [dateForm setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
        NSDate *prevLoginDate = [dateForm dateFromString:timeStampString];
        
        NSTimeInterval elapsedTime = [prevLoginDate timeIntervalSinceNow];
        elapsedTime = (elapsedTime < 0) ? -elapsedTime : elapsedTime;
        
        if (elapsedTime < Login_Token_Expiry_period) {
            return YES;
        }
    }
    
    return NO;
}

- (void) loginSuccessfulWithUserdetails:(User *) userDetails password:(NSString *) pWord{
    
    self.sessionUser = userDetails;
    
    NSDate *loginDate = [NSDate date];
    NSString *dateString = [loginDate description];
    
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.username forKey:Login_Token_Username_Key];
    [[NSUserDefaults standardUserDefaults] setValue:pWord forKey:Login_Token_Password_Key];
    [[NSUserDefaults standardUserDefaults] setValue:dateString forKey:Login_Token_Timestamp_Key];
    
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.userId forKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.client_id forKey:@"client_id"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.domain_id forKey:@"domain_id"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.type forKey:@"type"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.email forKey:@"email"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.first_name forKey:@"first_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.last_name forKey:@"last_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.state forKey:@"state"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.country forKey:@"country"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.mobile forKey:@"mobile"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.card_ext forKey:@"card_ext"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.client_name forKey:@"client_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.newsletter forKey:@"newsletter"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.clientDomainName forKey:@"clientDomainName"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    [self showhomeScreen];
}

- (void) logoutUserSession {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:Login_Token_Timestamp_Key];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:Login_Token_Username_Key];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:Login_Token_Password_Key];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:Login_Token_Timestamp_Key];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"client_id"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"domain_id"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"type"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"email"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"first_name"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"last_name"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"state"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"country"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"mobile"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"card_ext"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"client_name"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"newsletter"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"clientDomainName"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self showRegistrationScreen];
    
}


- (void) renewTheLoginToken {
    
    NSLog(@"show renewTheLoginToken");
    // Override point for customization after application launch.
    
    self.viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    
    
//    [self.viewController setProcessRenwalOfToken:YES];
    
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
}

- (void) showRegistrationScreen {
    
    // ** If Home Controller is present, Dealloc it
    if (self.homeViewController) {
        self.homeViewController = nil;
    }
    
    NSLog(@"show RegistrationScreen");
    // Override point for customization after application launch.
    
    self.viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
}

- (void) showhomeScreen {
    
    // ** If Login Controller is present, Dealloc it
    if (self.viewController) {
        self.viewController = nil;
    }
    NSLog(@"showing Home Screen");
    
    self.homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    
    self.window.rootViewController = self.homeViewController;
    [self.window makeKeyAndVisible];
    
}

- (NSString *) sessionUsername {
    NSString *uName = [[NSUserDefaults standardUserDefaults] objectForKey:Login_Token_Username_Key];
    return uName;
    
}

- (NSString *) sessionPassword {
    NSString *pWord = [[NSUserDefaults standardUserDefaults] objectForKey:Login_Token_Password_Key];
    return pWord;
}

- (NSString *) clientDomainName {
    if (sessionUser) {
        return sessionUser.clientDomainName;
    }
    return nil;
}

- (User *) retrieveUserDetailsFromUserDefaults {
    User *userdetails = [[User alloc] init];
    
    userdetails.username = [[NSUserDefaults standardUserDefaults] objectForKey:Login_Token_Username_Key];
    userdetails.userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
    userdetails.client_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"client_id"];
    userdetails.domain_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"domain_id"];
    userdetails.type = [[NSUserDefaults standardUserDefaults] objectForKey:@"type"];
    userdetails.email = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
    userdetails.first_name = [[NSUserDefaults standardUserDefaults] objectForKey:@"first_name"];
    userdetails.last_name = [[NSUserDefaults standardUserDefaults] objectForKey:@"last_name"];
    userdetails.state = [[NSUserDefaults standardUserDefaults] objectForKey:@"state"];
    userdetails.country = [[NSUserDefaults standardUserDefaults] objectForKey:@"country"];
    userdetails.mobile = [[NSUserDefaults standardUserDefaults] objectForKey:@"mobile"];
    userdetails.card_ext = [[NSUserDefaults standardUserDefaults] objectForKey:@"card_ext"];
    userdetails.client_name = [[NSUserDefaults standardUserDefaults] objectForKey:@"client_name"];
    userdetails.newsletter = [[NSUserDefaults standardUserDefaults] objectForKey:@"newsletter"];
    userdetails.clientDomainName = [[NSUserDefaults standardUserDefaults] objectForKey:@"clientDomainName"];
    
    return userdetails;
}


//====================================   LOCAL NOTIFICAIONS   =======================//
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    
    
    NSLog(@"didReceiveLocalNotification");
    application.applicationIconBadgeNumber = 0;
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Back To My Car" message:@"Parking is about to expire!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    NSError *error;
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/old_phone.m4r", [[NSBundle mainBundle] resourcePath]]];
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    player.numberOfLoops = 1;
    [player play];
    
    [alert show];
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    [self clearRemainder];
    self.temp = 0;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ParkingRemainderCount"];
    if (application.applicationIconBadgeNumber == 0) {
        [self clearRemainder];
        NSLog(@"clear....");
    }
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        [player stop];
        
    }
    
}

-(void)startCountDownTimerWithTime:(int)time andUILabel:(UILabel *)currentLabel
{
    
    countDownTime = time;
    actualTime = time;
    label = currentLabel;
    [self StartCountDownTimer];
}


-(void)invalidateCurrentCountDownTimer
{
    [self InvalidateCountDownTimer];
}

#pragma mark -
#pragma mark count Down Timer
-(void)InvalidateCountDownTimer
{
    NSLog(@"InvalidateCountDownTimer");
    countDownTime =actualTime;
    if (CountDownTimer!=nil)
    {
        if ([CountDownTimer isValid])
        {
            [CountDownTimer invalidate];
            
        }
        CountDownTimer=nil;
    }
}

-(void)StartCountDownTimer
{
    [self InvalidateCountDownTimer];
    CountDownTimer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(DecrementCounterValue) userInfo:nil repeats:YES];
    
    label.text=[NSString stringWithFormat:@"%02d:%02d:%02d", actualTime/3600, (actualTime % 3600) / 60, (actualTime %3600) % 60];
}

-(void)DecrementCounterValue
{
    if (countDownTime>0)
	{
        countDownTime--;
        x=countDownTime;
        
        label.text = [NSString stringWithFormat:@"%02d:%02d:%02d", countDownTime/3600, (countDownTime % 3600) / 60, (countDownTime %3600) % 60];
        
        NSLog(@"%@",label.text);
    }
    else
	{
        [self InvalidateCountDownTimer];
        label.text = @"";
    }
    
    
    
    
}

//============================================================================================//

//=====================================REMAINDER CORE DATA METHODS =================================================//


-(BOOL) addRemainder:(ParkingTimer *) remainder
{
    
    
    //  NSLog(@"addContact: %@",contact.name);
    // NSLog(@"addContact: %@",contact.number);
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *newRemainder = [NSEntityDescription insertNewObjectForEntityForName:@"ParkingTimer" inManagedObjectContext:context];
    [newRemainder setValue:remainder.location forKey:@"location"];
    [newRemainder setValue:remainder.parkedLocation forKey:@"parkedLocation"];
    [newRemainder setValue:remainder.parkedTime forKey:@"parkedTime"];
    [newRemainder setValue:remainder.latitude forKey:@"latitude"];
    [newRemainder setValue:remainder.longitude forKey:@"longitude"];
    
    NSLog(@"remainder.parkedTime is... %@",newRemainder);
    remndr++;
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding remainder failed %@", [error localizedDescription]);
        return NO;
        remndr--;
    }
    
    return YES;
    
}

- (BOOL) clearRemainder
{
    label.text = @"";
    NSLog(@"clearRemainder");
    NSManagedObjectContext *context = [self managedObjectContext];
    remndr--;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *delRemainder = [NSEntityDescription entityForName:@"ParkingTimer" inManagedObjectContext:context];
    [fetchRequest setEntity:delRemainder];
    // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@ && number==%@",contact.name,contact.number];
    // [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0)
    {
        NSManagedObject *cont = [fetchedObjects objectAtIndex:0];
        
        [context deleteObject:cont];
        NSLog(@"Remainder Object removed");
        
        NSError *error;
        if (![context save:&error])
        {
            NSLog(@"Removing remainder failed %@", [error localizedDescription]);
            
            // Removing object failed.
            return NO;
        }
        
        // Success
        return YES;
    }
    
    // There is no remainder as such specified by input param in parkingTimer.
    
    // There is no remainder as such specified by input param in parkingTimer.
    return NO;
}


- (ParkingTimer *) FetchRemainder
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *contact = [NSEntityDescription entityForName:@"ParkingTimer" inManagedObjectContext:context];
    [fetchRequest setEntity:contact];
    
    
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *remaindersArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects)
    {
        
        
        ParkingTimer *currRemainder = [[ParkingTimer alloc] init];
        currRemainder.location = [[info valueForKey:@"location"] copy];
        currRemainder.parkedLocation = [[info valueForKey:@"parkedLocation"] copy];
        currRemainder.parkedTime = [[info valueForKey:@"parkedTime"] copy];
        currRemainder.latitude = [[info valueForKey:@"latitude"] copy];
        currRemainder.longitude = [[info valueForKey:@"longitude"] copy];
        
        [remaindersArr addObject:currRemainder];
    }
    if([remaindersArr count] > 0)
    {
        return [remaindersArr objectAtIndex:0];
        
    }
    else
        return NULL;
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    
    NSString *str = [NSString stringWithFormat:@"%d",countDownTime];
    [[NSUserDefaults standardUserDefaults] setValue:str forKey:@"parkingTime"];
    
    // NSLog(@"applicationWillResignActive Closed countDownTime::%d",countDownTime);
    NSLog(@"applicationWillResignActive");
    [self InvalidateCountDownTimer];
    [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:@"closedTime"];
    //closedTime = [NSDate date];
    
    
    
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    NSLog(@"applicationDidBecomeActive");
    
    NSString *countDownTimeStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"parkingTime"];
    NSDate *currentDate = [NSDate date];
    
    NSTimeInterval distanceBetweenDates = [currentDate timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults] objectForKey:@"closedTime"]];
    
    // NSLog(@"time: %f", distanceBetweenDates);
    
    if(self.temp == 1 && [countDownTimeStr intValue]-distanceBetweenDates > 0 && countDownTime > 0 && [[NSUserDefaults standardUserDefaults] objectForKey:@"ParkingRemainderCount"]){
        NSLog(@"Timer started");
        [self startCountDownTimerWithTime:[countDownTimeStr intValue]-distanceBetweenDates andUILabel:label];
    }
    else{
        NSLog(@"Timer & Parking Time Values cleared");
        [self InvalidateCountDownTimer];
        label.text = @"";
        countDownTime = 0;
        application.applicationIconBadgeNumber = 0;
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"parkingTime"];
    }
    
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@"badgecount:::::::%d",badgecount);
    application.applicationIconBadgeNumber = badgecount;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -- Favorite methods

- (BOOL) addProductToFavorites:(Product *) product {
    
    if ([self productExistsInFavorites:product]) {
        // Product already exists. return.
        return NO;
    }
    
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *favoriteProduct = [NSEntityDescription insertNewObjectForEntityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [favoriteProduct setValue:product.productId forKey:@"productId"];
    [favoriteProduct setValue:product.productName forKey:@"productName"];
    [favoriteProduct setValue:product.productOffer forKey:@"productHighlight"];
    [favoriteProduct setValue:self.sessionUser.userId forKey:@"userId"];
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Adding product failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}

- (BOOL) removeProductFromfavorites:(Product *) product {
    
    if ([self productExistsInFavorites:product]) {
        NSManagedObjectContext *context = [self managedObjectContext];
        
        // Fetch the said NSManagedObject(favoriteProduct)
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
        [fetchRequest setEntity:favProduct];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productId == %@ && userId==%@",product.productId,self.sessionUser.userId];
        [fetchRequest setPredicate:predicate];
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        if (fetchedObjects.count > 0) {
            NSManagedObject *favoriteProduct = [fetchedObjects objectAtIndex:0];
            
            [context deleteObject:favoriteProduct];
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Removing product failed %@", [error localizedDescription]);
                
                // Removing object failed.
                return NO;
            }
            
            // Success
            return YES;
        }
        
        // There is no Product as such specified by input param in favorites.
    }
    
    // There is no Product as such specified by input param in favorites.
    return NO;
}



- (BOOL) productExistsInFavorites:(Product *) product {
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [fetchRequest setEntity:favProduct];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productId==%@ && userId==%@",product.productId,self.sessionUser.userId];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0) {
        return YES;
    }
    
    return NO;
}

- (NSArray *) myFavoriteProducts {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [fetchRequest setEntity:favProduct];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId==%@",self.sessionUser.userId];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *productsArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects) {
        NSLog(@"Product Name: %@", [info valueForKey:@"productName"]);
        NSLog(@"Product Id: %@", [info valueForKey:@"productId"]);
        
        Product *pr = [[Product alloc] init];
        pr.productId = [[info valueForKey:@"productId"] copy];
        pr.productName = [[info valueForKey:@"productName"] copy];
        pr.productOffer = [[info valueForKey:@"productHighlight"] copy];
        [productsArr addObject:pr];
    }
    
    return productsArr;
}

//=================== NOTICEBOARD COREDATA METHODS =======================//

- (BOOL) addNoticeBoardID:(NoticeBoard *) nb
{
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *obj = [NSEntityDescription insertNewObjectForEntityForName:@"NoticeBoard" inManagedObjectContext:context];
    [obj setValue:nb.notice_Id forKey:@"noticeId"];
    [obj setValue:[NSString stringWithFormat:@"%d",nb.index] forKey:@"noticeIndex"];
    
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding NoticeBoard_ID failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}


- (NSArray *) noticeBoardIds
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"NoticeBoard" inManagedObjectContext:context];
    [fetchRequest setEntity:entityDescription];
    
    //  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Id==%@",self.sessionUser.userId];
    //  [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *noticeBoardArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects)
    {
        
        //NSLog(@"NoticeBoard Id: %@", [info valueForKey:@"noticeId"]);
        
        NoticeBoard *nb = [[NoticeBoard alloc] init];
        nb.notice_Id = [[info valueForKey:@"noticeId"] copy];
        NSString *str = [[info valueForKey:@"noticeIndex"] copy];
        nb.index = [str intValue];
        
        [noticeBoardArr addObject:nb];
    }
    
    return noticeBoardArr;
}


- (BOOL) removeNoticeboard:(NoticeBoard *) nb
{
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Fetch the said NSManagedObject(favoriteProduct)
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"NoticeBoard" inManagedObjectContext:context];
    [fetchRequest setEntity:favProduct];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"noticeId == %@",nb.notice_Id];
    [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0)
    {
        NSManagedObject *favoriteProduct = [fetchedObjects objectAtIndex:0];
        
        [context deleteObject:favoriteProduct];
        
        NSError *error;
        if (![context save:&error])
        {
            NSLog(@"Removing NB Id failed %@", [error localizedDescription]);
            
            // Removing object failed.
            return NO;
        }
        
        // Success
        return YES;
    }
    
    
    
    // There is no ids as such specified by input param in NB.
    return NO;
}


- (BOOL) nbExistsInCoredata:(NoticeBoard *) nb
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *obj = [NSEntityDescription entityForName:@"NoticeBoard" inManagedObjectContext:context];
    [fetchRequest setEntity:obj];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"noticeId==%@",nb.notice_Id];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0)
    {
        return YES;
    }
    
    return NO;
}






/*
 - (BOOL) addNoticeBoardID:(NoticeBoard *) nb {
 
 
 NSManagedObjectContext *context = [self managedObjectContext];
 NSManagedObject *obj = [NSEntityDescription insertNewObjectForEntityForName:@"NoticeBoard" inManagedObjectContext:context];
 [obj setValue:nb.notice_Id forKey:@"noticeId"];
 
 
 NSError *error;
 if (![context save:&error]) {
 NSLog(@"Adding NoticeBoard_ID failed %@", [error localizedDescription]);
 return NO;
 }
 
 return YES;
 }
 
 
 - (NSArray *) noticeBoardIds {
 
 NSManagedObjectContext *context = [self managedObjectContext];
 NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
 NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"NoticeBoard" inManagedObjectContext:context];
 [fetchRequest setEntity:entityDescription];
 
 //  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Id==%@",self.sessionUser.userId];
 //  [fetchRequest setPredicate:predicate];
 
 NSError *error;
 NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
 NSMutableArray *noticeBoardArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
 
 for (NSManagedObject *info in fetchedObjects) {
 
 // NSLog(@"NoticeBoard Id: %@", [info valueForKey:@"noticeId"]);
 
 NoticeBoard *nb = [[NoticeBoard alloc] init];
 nb.notice_Id = [[info valueForKey:@"noticeId"] copy];
 
 [noticeBoardArr addObject:nb];
 }
 
 return noticeBoardArr;
 }
 
 
 - (BOOL) removeNoticeboard:(NoticeBoard *) nb {
 
 
 NSManagedObjectContext *context = [self managedObjectContext];
 
 // Fetch the said NSManagedObject(favoriteProduct)
 NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
 NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"NoticeBoard" inManagedObjectContext:context];
 [fetchRequest setEntity:favProduct];
 NSPredicate *predicate = [NSPredicate predicateWithFormat:@"noticeId == %@",nb.notice_Id];
 [fetchRequest setPredicate:predicate];
 NSError *error;
 NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
 
 if (fetchedObjects.count > 0) {
 NSManagedObject *favoriteProduct = [fetchedObjects objectAtIndex:0];
 
 [context deleteObject:favoriteProduct];
 
 NSError *error;
 if (![context save:&error]) {
 NSLog(@"Removing NB Id failed %@", [error localizedDescription]);
 
 // Removing object failed.
 return NO;
 }
 
 // Success
 return YES;
 }
 
 
 
 // There is no ids as such specified by input param in NB.
 return NO;
 }
 
 
 - (BOOL) nbExistsInCoredata:(NoticeBoard *) nb {
 
 NSManagedObjectContext *context = [self managedObjectContext];
 NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
 
 NSEntityDescription *obj = [NSEntityDescription entityForName:@"NoticeBoard" inManagedObjectContext:context];
 [fetchRequest setEntity:obj];
 
 NSPredicate *predicate = [NSPredicate predicateWithFormat:@"noticeId==%@",nb.notice_Id];
 [fetchRequest setPredicate:predicate];
 
 NSError *error;
 NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
 
 if (fetchedObjects.count > 0) {
 return YES;
 }
 
 return NO;
 }
 
 */






//==========================================================================================//
//===================================== PUSH NOTIFICATIONS METHODS==========================//

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken
{
    // Store the deviceToken in the current installation and save it to Parse.
    
    //NSArray *channelsArray = [[NSArray alloc]initWithObjects:@"vaisoftChannel", nil];
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:newDeviceToken];
    [currentInstallation saveInBackground];
    [currentInstallation addUniqueObject:@"Vaisoft" forKey:@"channels"];
    //[currentInstallation setChannels:channelsArray];
    //[PFPush subscribeToChannelInBackground:@"vaisoftChannel"];
    
    
    NSString *myString = [[NSString alloc] initWithData:newDeviceToken encoding:NSUTF8StringEncoding];
    NSLog(@"DEVICE TOKEN:::%@",myString );
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    [PFPush handlePush:userInfo];
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    if ([error code] == 3010)
    {
        NSLog(@"Push notifications don't work in the simulator!");
    }
    else { NSLog(@"didFailToRegisterForRemoteNotificationsWithError: %@", error); }
}




//===================================== END OF PUSH NOTIFICATIONS METHODS==========================//

//======================================
#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
/*
 - (NSManagedObjectModel *)managedObjectModel
 {
 if (_managedObjectModel != nil) {
 return _managedObjectModel;
 }
 NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"FavoritesDataModel 2" withExtension:@"momd"];
 _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
 return _managedObjectModel;
 }
 */

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"FavoritesDataModel.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


@end
