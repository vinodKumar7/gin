//
//  MenuTableCell.h
//  GrabItNow
//
//  Created by vairat on 04/05/15.
//  Copyright (c) 2015 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *menuItemImage;
@property (strong, nonatomic) IBOutlet UILabel *menuItemName;

@end
