//
//  Category.m
//  GrabItNow
//
//  Created by MyRewards on 12/22/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "Category.h"

@implementation Category

@synthesize catId;
@synthesize catName;
@synthesize catDescription;
@synthesize catImgURLString;

@end
