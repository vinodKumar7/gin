//
//  KeyboardViewController.m
//  PDKTStickySectionHeadersCollectionViewLayoutDemo
//
//  Created by vairat on 17/03/15.
//  Copyright (c) 2015 Produkt. All rights reserved.




#import <UIKit/UIKit.h>
#import "KeyboardViewController.h"
#import "ZenKeyboard.h"
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"



@interface KeyboardViewController ()

{
    
    ASIFormDataRequest *redeemRequest;
    AppDelegate *appDelegate;
    ZenKeyboard *keyboard;
    UIView *loadingView;
}
@end

@implementation KeyboardViewController
@synthesize presentTextField, currentTextField, accessoryView,merchantMustLabel, product_ID, product_imageView, lblCoffeeFree, lblProductName,productName,productOffer,productImage,redeemPassword,product_Image, deviceID,multipleRedeemCount,isLastStamp;



- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChangeOneCI:)
                                                           name:UITextFieldTextDidChangeNotification
                                                           object:presentTextField];
    
    self.view.userInteractionEnabled = YES;
    merchantMustLabel.backgroundColor=[UIColor colorWithRed:236.0/255.0 green:0.0/255.0 blue:140/255.0 alpha:1.0];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    

}
- (void) viewWillAppear:(BOOL)animated
{
    product_imageView.image             = product_Image;
    product_imageView.layer.borderWidth = 2.0;
    product_imageView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    keyboard = [[ZenKeyboard alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height/1.7, self.view.bounds.size.width, 276)];
    
    keyboard.textField    = presentTextField;
    presentTextField.text = @"";
    [presentTextField becomeFirstResponder];
    
    
    
    if(self.isLastStamp)
        NSLog(@" LAST STAMP");
    else
        NSLog(@"NOT LAST STAMP");
}


#pragma mark- User Defined Methods

-(void)validatingPIN
{
    loadingView=[[UIView alloc]initWithFrame:self.view.bounds];
    loadingView.backgroundColor=[UIColor blackColor];
    UIActivityIndicatorView *activityIndicator;
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = CGRectMake(0.0, 0.0, 100.0, 100.0);
    activityIndicator.center = self.view.center;
    [activityIndicator startAnimating];
    presentTextField.text=@"";
    [loadingView addSubview: activityIndicator];
    [self.view addSubview:loadingView];
    
}



-(void)changeValuesOnKeyboard{}

-(void)textFieldTextDidChangeOneCI:(NSNotification *)notification
{
   
    if ([presentTextField.text length]== 4) {
        
        if ([presentTextField.text isEqualToString:redeemPassword]) {
            presentTextField.text=@"";
            
            NSString *urlString = [NSString stringWithFormat:@"%@redeemed.php",URL_Prefix];
            NSURL *url = [NSURL URLWithString:urlString];

            redeemRequest = [[ASIFormDataRequest alloc] initWithURL:url];
            [redeemRequest setPostValue:deviceID forKey:@"user_id"];
            [redeemRequest setPostValue:product_ID forKey:@"pid"];
            [redeemRequest setPostValue:@"24" forKey:@"cid"];
            [redeemRequest setPostValue:@"0.0"forKey:@"lat"];
            [redeemRequest setPostValue:@"0.0" forKey:@"lon"];
            [redeemRequest setPostValue:multipleRedeemCount forKey:@"count"];
            
            [redeemRequest setDelegate:self];
            [redeemRequest startAsynchronous];
        }
        else
            presentTextField.text=@"";
        
    }
    
}

#pragma mark- UIButton Action Methods

- (IBAction)backBtnPressed:(id)sender {
    
    [presentTextField resignFirstResponder];
    presentTextField.text=@"";
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    
}

#pragma mark- UITextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    self.currentTextField = textField;
    [textField setInputAccessoryView:accessoryView];
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (textField.tag==100) {
        return (newLength > 4) ? NO : YES;
    }
    else
        return (newLength > 3) ? NO : YES;
}



#pragma mark- Request Finished and Request Failed

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSLog(@"redeemRequest response %@",[request responseString]);
    
    NSString *str = [NSString stringWithString:[request responseString]];
    //int len = (int)[str length] - 1;
    NSString *subStr = [str substringWithRange:NSMakeRange(1, 7)];
    NSLog(@"subStr:: %@",subStr);
    
    if([subStr isEqualToString:@"success"] && isLastStamp){
        NSLog(@"appDelegate.isFromFreebies = YES in keyboardView");
    }
    
        [presentTextField resignFirstResponder];
        [self dismissViewControllerAnimated:NO completion:nil];
    
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                         message:@"Server Busy"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:@"Cancel",nil];
    [alert_view show];
    
   
    
}

-(void)viewDidDisappear:(BOOL)animated{
    
    
    isLastStamp = NO;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
