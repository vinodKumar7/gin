//
//  LoyaltyDetailViewController.h
//  CoffeeApp
//
//  Created by vairat on 28/03/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductListParser.h"


@interface LoyaltyDetailViewController : UIViewController

@property(nonatomic, strong)UIImage *productImage;
@property(nonatomic, strong)NSString *product_id;
@property(nonatomic, strong)NSString *product_Name;
@property(nonatomic, strong)IBOutlet UIButton *redeemButton;
- (IBAction)redeemBtnPressed:(id)sender;

@end
