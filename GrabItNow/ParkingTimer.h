//
//  ParkingTimer.h
//  TWU2
//
//  Created by vairat on 11/04/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface ParkingTimer : NSObject
{
    
}
@property (nonatomic, strong) NSString * location;
@property (nonatomic, strong) NSString * parkedLocation;
@property(nonatomic, strong)NSString * parkedTime;
@property(nonatomic, strong)NSString *latitude;
@property(nonatomic,strong)NSString *longitude;
@property(nonatomic, strong)NSString * when;
@end
